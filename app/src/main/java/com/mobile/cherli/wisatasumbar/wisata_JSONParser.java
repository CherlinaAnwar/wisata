package com.mobile.cherli.wisatasumbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Cherly on 18/02/2016.
 */
public class wisata_JSONParser {
    /** Menerima  JSONObject dan mengembalikannya menjadi list */
    public List<HashMap<String,String>> parse(JSONObject jObject){

        JSONArray jw = null;
        try{
            /** Menerima semua element dari data array yang ada diapi server */
            jw = jObject.getJSONArray("data");
        }catch(JSONException e){
            e.printStackTrace();
        }

        return getwisata(jw);
    }

    private List<HashMap<String, String>> getwisata(JSONArray jw){
        int wisataCount = jw.length();
        List<HashMap<String, String>> wisataList = new ArrayList<HashMap<String,String>>();
        HashMap<String,String> wisata = null;


        for(int i=0; i<wisataCount;i++){
            try{
                wisata = getWisata((JSONObject)jw.get(i));
                wisataList.add(wisata);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
        return wisataList;
    }


    private HashMap<String, String> getWisata(JSONObject jWisata){

        HashMap<String, String> wisata = new HashMap<String, String>();
        String idwisata = "";
        String namawisata = "";
        String latitude = "";
        String longitude = "";
        String keterangan="";
        String kota = "";

        try{

            if(!jWisata.isNull("id_wisata")){
                idwisata = jWisata.getString("id_wisata");
            }if(!jWisata.isNull("nama_wisata")){ //
                namawisata = jWisata.getString("nama_wisata");
            }if(!jWisata.isNull("lat")){
                latitude = jWisata.getString("lat");
            }if(!jWisata.isNull("long")){
                longitude = jWisata.getString("long");
            }if(!jWisata.isNull("keterangan")){
                keterangan = jWisata.getString("keterangan");
            }if(!jWisata.isNull("nama_kota")){
                kota=jWisata.getString("nama_kota");
            }

           wisata.put("id_wisata", idwisata);
            wisata.put("nama_wisata", namawisata);
            wisata.put("lat", latitude);
            wisata.put("long", longitude);
            wisata.put("keterangan", keterangan);
            wisata.put("nama_kota", kota);


        }catch(JSONException e){
            e.printStackTrace();
        }
        return wisata;
    }
}
