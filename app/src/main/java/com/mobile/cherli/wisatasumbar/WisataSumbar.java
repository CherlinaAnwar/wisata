package com.mobile.cherli.wisatasumbar;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
/**
 * Created by Cherly on 18/02/2016.
 */
public class WisataSumbar extends FragmentActivity {
    public GoogleMap map;

    public static final LatLng LOCATION_PADANG = new LatLng(-0.9652668, 100.39675);
    private static final String TAG_ID="id_wisata";
    private static final String TAG_NAMA="nama_wisata";
    private static final String TAG_LAT="lat";
    private static final String TAG_LONG="long";
    private static final String TAG_KET="keterangan";
    private static final String TAG_KOTA="nama_kota";

    private String idwisata,namawisata,latwisata,longwisata,ketwisata,kota;
    HashMap<String, HashMap>extraMarkerInfo = new HashMap<String, HashMap>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lokasi_wisata);

        map =((MapFragment)getFragmentManager().findFragmentById(R.id.maps)).getMap();
        CameraUpdate dekat = CameraUpdateFactory.newLatLngZoom(LOCATION_PADANG, 10);
        map.animateCamera(dekat);
       //  map.addMarker(new MarkerOptions().position(LOCATION_PADANG));
       // map.setMyLocationEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);

      new RetrieveTask().execute();
    }

    private class RetrieveTask extends AsyncTask<Void, Void, String> {
        public void execute() {
        }

        @Override
        protected String doInBackground(Void... params) {
            String surl="http://10.0.3.2/wisatasumbar/lokasi.php";
            //String surl="http://praproyek.esy.es/wisatasumbar/lokasi.php";
            URL url=null;
            StringBuffer sb=new StringBuffer();
            try{
                url = new URL(surl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream iStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
                String line = "";
                while( (line = reader.readLine()) != null){
                    sb.append(line);
                }
            }catch(MalformedURLException e){
                e.printStackTrace();
            }catch(IOException e){
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result){
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Void, List<HashMap<String, String>>> {

        public void execute(String result) {
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... params) {
            wisatalokasi_JSONParser wisata_Parser = new wisatalokasi_JSONParser();
            JSONObject json = null;
            try{
                json = new JSONObject(params[0]);
            }catch(JSONException e){
                e.printStackTrace();
            }
            List<HashMap<String, String>> wisataList = wisata_Parser.parse(json);
            return wisataList;
        }

        protected void onPostExecute(List<HashMap<String, String>> result){
            for(int i=0; i<result.size(); i++){
                HashMap<String, String> wisata = result.get(i);
                latwisata = wisata.get("lat");
                longwisata = wisata.get("long");

                LatLng latlng = new LatLng(Double.parseDouble(wisata.get("lat")), Double.parseDouble(wisata.get("long")));


             //idwisata = new String(wisata.get("id_wisata"));
                namawisata = new String(wisata.get("nama_wisata"));
                //latwisata = new String(wisata.get("lat"));
               // longwisata = new String(wisata.get("long"));
               // ketwisata = new String(wisata.get("keterangan"));
                //kota = new String(wisata.get("nama_kota")); */


                System.out.println("lat " + latwisata + " " + "long" + longwisata);

              // map.addMarker(new MarkerOptions().position(latlng));
                // create marker
               //MarkerOptions marker = new MarkerOptions().position(latlng).title("Hello Maps ");
               //  MarkerOptions marker=new MarkerOptions().position(new LatLng(Double.parseDouble(wisata.get("lat")),Double.parseDouble(wisata.get("long"))));
// adding marker
                //map.addMarker(marker);
               TambahMarker(latlng, namawisata);


            }
        }
    }

    private void TambahMarker(LatLng latlng, String namawisata){

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latlng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        markerOptions.title(namawisata);
        map.addMarker(markerOptions);


/*        Marker marker = map.addMarker(new MarkerOptions().position(latlng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(namawisata)); */

   /* HashMap data = new HashMap();
        data.put(TAG_ID, Idwisata);
        data.put(TAG_NAMA, namawisata);
        data.put(TAG_LAT, lat);
        data.put(TAG_LONG,longitude);
        data.put(TAG_KET, ket);
        data.put(TAG_KOTA, kota);

        // Save this marker data in your previously made HashMap mapped to the marker ID. So you can get it back based on the marker ID

        extraMarkerInfo.put(marker.getId(),data);
*/
    }
}

