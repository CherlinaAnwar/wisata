package com.mobile.cherli.wisatasumbar;

/**
 * Created by Cherly on 19/02/2016.
 */
public class Wisata {
    private String idwisata;
    private String namawisata;
    private String lat;
    private String longtitude;
    private String ket;

    public String getIdwisata() {
        return idwisata;
    }

    public void setIdwisata(String idwisata) {
        this.idwisata = idwisata;
    }

    public String getNamawisata() {
        return namawisata;
    }

    public void setNamawisata(String namawisata) {
        this.namawisata = namawisata;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getNamakota() {
        return namakota;
    }

    public void setNamakota(String namakota) {
        this.namakota = namakota;
    }

    private String namakota;
}
